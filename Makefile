PROTO_FILE=gkn.proto
SRC_DIR=.
GO_OUT=./go

go:
	protoc --go_out=$(GO_OUT) $(PROTO_FILE)

clean:
	rm -rf $(GO_OUT)/*

.PHONY: go
